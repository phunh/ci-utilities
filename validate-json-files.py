#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Alexander Lohnau <alexander.lohnau@gmx.de>
# SPDX-License-Identifier: BSD-2-Clause

import os
import subprocess
import yaml
from components import CommonUtils

####
# Load the project configuration
####
# This consists of:
# 0) Global configuration
configuration = yaml.safe_load( open(os.path.join(CommonUtils.scriptsBaseDirectory(), 'config', 'global.yml')) )

# 1) Project/branch specific configuration contained within the repository
if os.path.exists('.kde-ci.yml'):
    localConfig = yaml.safe_load( open('.kde-ci.yml') )
    CommonUtils.recursiveUpdate( configuration, localConfig )

# Extract excluded files, used for tests that intentionally have broken files
excluded_files = ['compile_commands.json', 'ci-utilities']
excluded_files += configuration['Options']['json-validate-ignore']

# Find JSON files
files = []
for root, dirs, filenames in os.walk('.'):
    for filename in filenames:
        if filename.endswith('.json'):
            file_path = os.path.join(root, filename)
            if not any(excluded_file in file_path for excluded_file in excluded_files):
                files.append(file_path)

# "include" overrides the "ignore" files, so if a file is both included and excluded, it will be included
files += configuration['Options']['json-validate-include']

if files:
    files_option = ' '.join(files)
    print(f"Validating {files_option}")
    schemafile = os.path.join(os.path.dirname(__file__), 'resources', 'kpluginmetadata.schema.json')
    result = subprocess.run(['check-jsonschema', *files, '--schemafile', schemafile])
    # Fail the pipeline if command failed
    if result.returncode != 0:
        exit(1)

